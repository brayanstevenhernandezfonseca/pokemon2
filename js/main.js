document.addEventListener("DOMContentLoaded", function () {
    const pokeContent = document.getElementById('pokemonContent');
    const pokeForm = document.getElementById('searchPokemon');
    const pokeballButton = document.getElementById('pokeballButton');
    const imgPokeballButton =document.getElementById('imgPokeballButton') 
    const txtEscuchar =document.getElementById('txtEscuchar')

    let isListening = false;
    const artyom = new Artyom();

    pokeForm.addEventListener('submit', e => {
        e.preventDefault();
        const searchPokemon = document.getElementById('pokemon').value.toLowerCase();
        getPokemon(searchPokemon);

    });

    let comando = {
        question: "Diga Buscar",
        indexes: ["buscar *","encontrar *","atrapar *"],
        smart: true,
        action: function (index,pokemon) {
            getPokemon(pokemon)
            
        }
    }
    artyom.addCommands(comando)
    
    pokeballButton.addEventListener('click', () =>{
        artyom.initialize({
            
            lang: "es-ES",
            debug: true,
            continuous: false,
            listen: true
        });
       imgPokeballButton.classList.add("hidden")
       txtEscuchar.classList.remove("hidden")
    })



    const getPokemon = async (query) => {
        let url;
        if (!isNaN(query)) {
            url = `https://pokeapi.co/api/v2/pokemon/${query}`;
        } else {
            url = `https://pokeapi.co/api/v2/pokemon/${query.toLowerCase()}`;
        }

        try {
            const res = await fetch(url);
            const pokemon = await res.json();
            createPokemon(pokemon);
        } catch (error) {
            console.error('Error fetching Pokemon:', error);
            alert('No se pudo encontrar el Pokémon');
        }
        finally{
            imgPokeballButton.classList.remove("hidden")
            txtEscuchar.classList.add("hidden")

        }
    };

    const createPokemon = (pokemon) => {
        const pokemonEl = document.createElement('div');
        pokemonEl.classList.add('pokemon');

        const poke_types = pokemon.types.map(type => type.type.name);
        const type = main_types.find(type => poke_types.indexOf(type) > -1);
        const name = pokemon.name[0].toUpperCase() + pokemon.name.slice(1);
        const color = colors[type];

        pokemonEl.style.backgroundColor = color;

        const pokeInnerHTML = `
            <div class="img-container">
                <img src="${pokemon.sprites.front_default}" alt="${name}" />
            </div>
            <div class="info">
                <span class="number">#${pokemon.id.toString().padStart(3, '0')}</span>
                <h3 class="name">${name}</h3>
                <small class="type">Tipo: <span>${type}</span></small>
            </div>
        `;
        pokemonEl.innerHTML = pokeInnerHTML;

        pokeContent.innerHTML = ''; // Limpiar contenido anterior
        pokeContent.appendChild(pokemonEl);
    };

    const colors = {
        fire: '#FFA05D',
        grass: '#8FD594',
        electric: '#FFE43B',
        water: '#7E97C0',
        ground: '#CAAC4D',
        rock: '#90642D',
        poison: '#9D5B9B',
        bug: '#EAFD71',
        dragon: '#97b3e6',
        psychic: '#FF96B5',
        flying: '#CDCDCD',
        fighting: '#FF5D5D',
        normal: '#FFFFFF'
    };

    const main_types = Object.keys(colors);
});
